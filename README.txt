AK Carousel
--------------------
A Drupal 7 module


Author
--------------------
Ranieri Machado 
hanietti@gmail.com
ranierimachado.com


Description
--------------------
The AK Carousell module generates sliding panels of nodes content.
It can be configured for generate one carousel for per content type.

The carousel is compound by 3 div wrappers:

                      [=========================================]
    Titles ------>    [| Title 1 | Title 2 | Title 3 | Title 4 |]
                      [=========================================]
             |--->    [ ---------                               ]
             |        [ |       |  LOREM IPSUM                  ]
             |        [ | Image |                               ]
             |        [ |       |  Nulla vel leo sed sapien     ]
    Sliding  |        [ ---------  accumsan commodo ac a dui.   ]
    Content -|        [ Nulla est erat, luctus non sem a,       ]
             |        [ mattiscommodo ante. In gravida dolor    ]
             |        [ lorem, vitaeconsequat magna egestas     ]
             |        [ quis.                                   ]
             |        [                                         ]
             |--->    [                                   More  ]
                      [=========================================]
    Arrows ------>    [ <-                                   -> ]
                      [=========================================]

Each of these div wrappers can have its own custom class defined 
on the module settings page: admin/config/content/ak_carousel.

Installation:
--------------------
Install as usual, see http://drupal.org/documentation/install/modules-themes/modules-7 for further information.


