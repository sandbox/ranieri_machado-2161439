<?php

/*
 *  
 *
 */

function ak_carousel_admin_settings() {    
    
    menu_rebuild();
    
    $node_names = node_type_get_names();
    $default_values = variable_get('ak_carousel_content_types', FALSE);
    if (!$default_values) {
        $default_values = array();
        for ($i=1; $i<=count($node_names); $i++) {
            $default_values[] = FALSE;
        }
    }
    
    // CONTENT fieldset
    $form['ak_carousel_contents'] = array(
        '#title' => t('Content'),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );         
    $form['ak_carousel_contents']['ak_carousel_content_types'] = array(
        '#title' => t('Content types to be used on AK Carousel.'),        
        '#type' => 'checkboxes',
        '#options' => $node_names,
        '#default_value' => $default_values,
        '#description' => t(''),   
    );     
    $form['ak_carousel_contents']['ak_carousel_first_argument'] = array(
        '#type' => 'textfield',
        '#title' => t('First argument of the path'),
        '#default_value' => variable_get('ak_carousel_first_argument', 'carousel'),
        '#description' => t('The path to the carousel is compound by two arguments.
          The second one is the machine name of the content type.<br /> 
          So if the first argument is "my-carousel" and the "Article" content 
          type is chosen the path will be "my-carousel/article".'),   
    );     
    $form['ak_carousel_contents']['ak_carousel_body_part'] = array(
        '#type' => 'radios',
        '#options' => array(0 => t('Summary'), 1 => t('Full content')),
        '#default_value' => variable_get('ak_carousel_body_part', 0),
        '#title' => t('Body part'),   
        '#description' => '',   
    );      
    
    // SLIDING CONFIGURATION fieldset
    $form['ak_carousel_sliding_setup'] = array(
        '#title' => t('Sliding configuration'),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );    
    $form['ak_carousel_sliding_setup']['ak_carousel_interval'] = array(
       '#type' => 'select',
       '#title' => t('Interval between each sliding step'),
       '#options' => drupal_map_assoc(range (10, 200, 10)),
       '#default_value' => variable_get('ak_carousel_interval', 20),
       '#description' => t('Miliseconds'),
    );
    $form['ak_carousel_sliding_setup']['ak_carousel_steps'] = array(
       '#type' => 'select',
       '#title' => t('Number of transition steps'),
       '#options' => drupal_map_assoc(range (10, 100, 5)),
       '#default_value' => variable_get('ak_carousel_steps', 50),
       '#description' => t('Steps'),
    );    
    
    // REGIONS fieldset
    $form['ak_carousel_regions'] = array(
        '#title' => t('Regions'),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    ); 
    $form['ak_carousel_regions']['ak_carousel_height'] = array(
       '#type' => 'select',
       '#title' => t('Height of the scrolling panel'),
       '#options' => drupal_map_assoc(range (50, 600, 5)),
       '#default_value' => variable_get('ak_carousel_height', 300),
       '#description' => t('Pixels'),
    );       
    $form['ak_carousel_regions']['ak_carousel_regions'] = array(
        '#type' => 'radios',
        '#options' => array(1 => t('Show only titles menu on top'), 2 => t('Show only arrows on bottom'), 0 => t('Show both')),
        '#default_value' => variable_get('ak_carousel_regions', 0),
        '#title' => t('Menus and arrows'),   
        '#description' => '',   
    );     
    // CUSTOM CLASSES fieldset
    $form['ak_carousel_regions']['ak_custom_classes'] = array(
        '#title' => t('Custom Classes'),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    ); 
    $form['ak_carousel_regions']['ak_custom_classes']['ak_carousel_menu_class'] = array(
        '#type' => 'textfield',
        '#title' => t('Top menu '),
        '#default_value' => variable_get('ak_carousel_menu_class', ''),
        '#description' => t(''),   
    );       
    $form['ak_carousel_regions']['ak_custom_classes']['ak_carousel_content_class'] = array(
        '#type' => 'textfield',
        '#title' => t('Content '),
        '#default_value' => variable_get('ak_carousel_content_class', ''),
        '#description' => t(''),   
    );       
    $form['ak_carousel_regions']['ak_custom_classes']['ak_carousel_arrows_class'] = array(
        '#type' => 'textfield',
        '#title' => t('Bottom Arrows'),
        '#default_value' => variable_get('ak_carousel_arrows_class', ''),
        '#description' => t(''),   
    );  
    // DESIGNING MODE fieldset
    $form['ak_carousel_regions']['ak_designing_mode'] = array(
        '#title' => t('Designing Mode'),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    ); 
    $form['ak_carousel_regions']['ak_designing_mode']['ak_carousel_display_outline'] = array(
        '#type' => 'checkbox',
        '#title' => t('Display Outline'),
        '#default_value' => variable_get('ak_carousel_display_outline', FALSE),
        '#description' => t('Stands out the node\'s content wrapper for designing purposes.'),   
    );
    $form['ak_carousel_regions']['ak_designing_mode']['ak_carousel_outline_color'] = array(
        '#type' => 'textfield',
        '#title' => t('Outline color '),
        '#default_value' => variable_get('ak_carousel_outline_color', '#888888'),
        '#description' => t('Use <i>#RRGGBB</i> notation.'),   
    );       
        
    // "MORE" LINK fieldset
    $form['ak_carousel_more_link'] = array(
        '#title' => t('"More" link'),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );      
    $form['ak_carousel_more_link']['ak_carousel_display_more'] = array(
        '#type' => 'checkbox',
        '#title' => t('Display "More" link'),
        '#default_value' => variable_get('ak_carousel_display_more', FALSE),
        '#description' => t(''),   
    ); 
    $form['ak_carousel_more_link']['ak_carousel_text_more'] = array(
        '#type' => 'textfield',
        '#title' => t('Text to be displayed'),
        '#default_value' => variable_get('ak_carousel_text_more', 'More'),
        '#description' => t(''),   
    ); 
    
    return system_settings_form($form);  
    
}

