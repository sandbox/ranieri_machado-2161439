/* 
 * Script that handles the sliding of the carousel.
 * 
 * Variables whose initial values ??were previously defined in the inline 
 * javascript added by the "ak_carousel_build_event_triggers()" function 
 * in the "ak_carousel.module":
 
 * - akPanelHeight: Height in pixels of the carousel panel,
 * - akIngterval: Time in milliseconds of the interval between each sliding step,
 * - akSlideJump: Number of transition steps contained in a single transition.
 * - akShowOutline: A flag indicating whether or not display an outline surrounding the content wrapper.
 * - akOutlineColor: The color of the outline;
 * - akEventTriggers: Array of elements ids of the divs containing the nodes contents.
 *
 * Variables whose initial values ??are defined in this scope
 * 
 * - akBlockWidth: Numeric value of the width in pixels of the content area. It will posteriorly used for calculate the vertical shifting.
 * - akWrapperWidth: Width of the div that wrappers all nodes contents: number of nodes multiplied by the content area width plus additional 10 pixels.
 * 
 * 
 */
(function ($) {
  $(document).ready(function () {
      
      
    //sets the panel height  
    $('div.ak-row').css('height', akPanelHeight + 'px');  
    $('div#ak-rows-wrapper').css('height', (akPanelHeight + 2) + 'px');  
    $('div#ak-rows-frame').css('height', (akPanelHeight + 3) + 'px');      
      
    var akContentWidth = $('#ak-rows-frame').css('width');
    var akBlockWidth  = akContentWidth.replace(/\D/g, '' );  
    //Number of nodes multiplied by the content area width plus additional 10 pixels
    var akWrapperWidth = (akBlockWidth * akEventTriggers.length) + 10;
    var currentCol = 0;
    var lastCol = akEventTriggers.length-1;
    var isSliding = false;

    if (akShowOutline) {
      $('div.ak-content').css('outline', '1px solid' + akOutlineColor );
    }

    checkArrows();
    
    $('#ak-rows-wrapper').css('width', akWrapperWidth + 'px');     
    $('.ak-row').css('width', akBlockWidth + 'px');   
    
    for (var i=0; i<=lastCol; i++){
        $(akEventTriggers[i]).click(function(event){akSlide(event.target);});          
    }         

    function akSlide(eventTarget) {
        if (isSliding) return;
        var s = $(eventTarget).attr('id');   // ak-title-5
        var a = s.split('-');                // array('ak', 'title', '5')
        var i = Number(a[a.length-1]);       // 5
        var currentPos = getCurentPos();
        var nextPos = i * akBlockWidth * -1;
        if (nextPos!==currentPos) {
            var slideJump = Math.abs(currentPos-nextPos)/akSlideJump;
            if (nextPos<currentPos) {
              slideBlockToLeft(currentPos, nextPos, slideJump);                             
            } else {
              slideBlockToRight(currentPos, nextPos, slideJump);               
            }
        }        
        currentCol = i;
        checkArrows();
    }    
    
    function checkArrows() {
        if (currentCol==0) {
           $('#ak-arrow-left').hide(); 
        }
        else {
           $('#ak-arrow-left').show(); 
        }
        if (currentCol==lastCol) {
           $('#ak-arrow-right').hide(); 
        }
        else {
           $('#ak-arrow-right').show();            
        }        
    }

    function slideBlockToLeft(posIni, posFin, slideJump) {
        posIni = posIni-slideJump; 
        if (posIni>posFin) { 
            isSliding = true;
            $('#ak-rows-wrapper').css('left', posIni + 'px');
            setTimeout(function(){slideBlockToLeft(posIni, posFin, slideJump);}, akIngterval);
        } 
        else { 
            isSliding = false;
            $('#ak-rows-wrapper').css('left', posFin + 'px');
        } 
    }
    
    function slideBlockToRight(posIni, posFin, slideJump) {
        posIni = posIni+slideJump;         
        if (posIni<posFin) { 
            isSliding = true;
            $('#ak-rows-wrapper').css('left', posIni + 'px');
            setTimeout(function(){slideBlockToRight(posIni, posFin, slideJump);}, akIngterval);          
        }
        else {
            isSliding = false;
            $('#ak-rows-wrapper').css('left', posFin + 'px'); 
        }
    }       
    
    function getCurentPos() {        
        var currentPos = $('#ak-rows-wrapper').css('left');  // 123px
            currentPos = currentPos.replace(/\D/g, '' );     // 123
            currentPos = currentPos * -1;                    // -123
        return currentPos;    
    }
    
    $('#ak-arrow-left').click(function(){
        if (isSliding) return;
        if (currentCol>0) {
            var currentPos = getCurentPos();
            var nextCol = currentCol-1;
            var nextPos = nextCol * akBlockWidth * -1;        
            var slideJump = Math.abs(currentPos-nextPos)/akSlideJump;
            slideBlockToRight(currentPos, nextPos, slideJump);                             
            currentCol = nextCol;
            checkArrows();
        }
    });
    
    $('#ak-arrow-right').click(function(){
        if (isSliding) return;
        if (currentCol<lastCol)  {
            var currentPos = getCurentPos();        
            var nextCol = currentCol+1;
            var nextPos = nextCol * akBlockWidth * -1;        
            var slideJump = Math.abs(currentPos-nextPos)/akSlideJump;
            slideBlockToLeft(currentPos, nextPos, slideJump);   
            currentCol = nextCol;
            checkArrows();
        }
    });
       
  });  
  
 
  

  
})(jQuery);

